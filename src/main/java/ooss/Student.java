package ooss;

public class Student extends Person {

    public boolean isleader = false;

    public Student(int id, String name, int age) {
        super(id, name, age);
        this.number = null;
    }

    public String introduce() {
        if(isleader){
            return String.format("My name is %s. I am %s years old. I am a student. I am the leader of class %s.",name,age,number.getNumber());
        }else if (number!=null){
            return String.format("My name is %s. I am %s years old. I am a student. I am in class %s.",name,age,number.getNumber());
        }else{
            return String.format("My name is %s. I am %s years old. I am a student.",name,age);
        }
    }

    public void join(Klass klass){
        this.number = klass;
    }

    public boolean isIn(Klass klass){
        return klass.equals(number);
    }

    public void update(Student student) {
        if (student.equals(this)) {
            System.out.println("I am " + name + ", student of Class " + student.number.getNumber() + ". I know " + student.name + " become Leader.");
        }
    }

}
