package ooss;

import java.util.Objects;

public class Person {

    protected int id;

    protected String name;

    protected int age;

    protected Klass number;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return String.format("My name is %s. I am %s years old.",name,age);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Person person = (Person) obj;
        return id == person.id;
    }

    public int hashCode() {
        return Objects.hash(number);
    }

}
