package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {

    private List<Klass> classes;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.classes = new ArrayList<>();
    }

    public String introduce() {
        StringBuffer number = new StringBuffer();
        for (Klass klass : classes) {
            number.append(klass.getNumber()).append(", ");
        }
        if(number.length()>1){
            number.deleteCharAt(number.length()-1);
            number.deleteCharAt(number.length()-1);
            return String.format("My name is %s. I am %s years old. I am a teacher. I teach Class %s.",name,age,number);
        }
        return String.format("My name is %s. I am %s years old. I am a teacher.",name,age);
    }

    public boolean assignTo(Klass klass){
        classes.add(klass);
        return true;
    }
    public boolean belongsTo(Klass klass){
        return classes.contains(klass);
    }

    public boolean isTeaching(Student student){
        for (Klass klass : classes) {
            if(student.number.getNumber()==klass.getNumber()){
                return true;
            }
        }
        return false;
    }
    public void update(Student student) {
        System.out.println("I am " + name + ", teacher of Class " + student.number.getNumber() + ". I know " + student.name + " become Leader.");
    }
}
