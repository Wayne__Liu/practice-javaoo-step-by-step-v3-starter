## **O**：

- I conducted a Code Review on the code at the beginning of this morning. In the code review section, learn the importance of method naming and Java Stream related knowledge, such as map(), filter(), reduce(), etc., to demonstrate the elegance of the code.

- Then we learned a lot about Java Stream. Before that, although I occasionally had some useful knowledge, my understanding of it was not very clear. After today's study, there was a significant change in my understanding. It greatly improved the convenience of development and increased the aesthetics of the code, making it a valuable tool to learn.
- Part of learning about Java OO, learning its ideas and code implementation

## **R：**

- The basic knowledge of Java is relatively weak


## **I：**

- adaptation


## **D：**

- Able to solve all classroom knowledge during class time