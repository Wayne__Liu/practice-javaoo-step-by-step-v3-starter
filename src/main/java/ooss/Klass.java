package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {

    private int number;

    private List<Student> students;

    private Teacher teacher;

    public Klass(int number) {
        this.number=number;
        this.students = new ArrayList<>();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Klass klass = (Klass) obj;
        return number == klass.number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if(student.number==null||this.number!=student.number.getNumber()){
            System.out.println("It is not one of us.");
        }else {
            student.isleader=true;
            notifyObservers(student);
        }
    }

    public boolean isLeader(Student student) {
        return student.isleader;
    }

    public void attach(Student student) {
        students.add(student);
    }
    public void attach(Teacher teacher) {
        this.teacher=teacher;
    }

    private void notifyObservers(Student student) {
        for (Student observer : students) {
            observer.update(student);
        }
        if (teacher != null) {
            teacher.update(student);
        }
    }
}
